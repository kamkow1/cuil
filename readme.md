# Cuil - C UI Library

Cuil is a simple header-only C library for writing platform agnostic UIs.

# Examples

To see what an example application might look like, see the `examples/` folder.
It contains a small demo for two platforms - Raylib and WebAssembly.
The implementations are located inside `load.js` for WebAssembly and `cuil_raylib.c` for Raylib.

## Building the examples

In order to build an example you need to use Mibs (mini build system).

Bootstrap Mibs:
```console
$ cc -o mibs mibs.c
```

Build for a specified platform:
```console
$ ./mibs -C=build -platform=<platform>
```

valid platform names are:

- `raylib`
- `web`

If you have changed something in the build program (`mibs.c`), there's no need to manually
invoke the C compiler. Simply run mibs as `./mibs <some other options...>` and it will
automatically recompile itself.

