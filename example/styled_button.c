#include <stddef.h>
#define CUIL_IMPL
#include "cuil.h"

#ifdef CUIL_PLATFORM_RAYLIB
#include "raylib.h"
#endif

#define WIDTH  800
#define HEIGHT 600

Cuil_Button* test_button = 0;

void app_update(float dt)
{
    cuil_clear_background(0x181818FF);
    
    if (cuil_button_is_clicked(test_button)) {
        test_button->flip_color = true;
    } else {
        test_button->flip_color = false;
    }

    cuil_draw_button(test_button);
}

int main(void)
{
    test_button = cuil_make_button(100, 100, 100, 40, "Hello!");

#if defined(CUIL_PLATFORM_WEB)
    cuil_set_app_update_func(app_update);
#elif defined(CUIL_PLATFORM_RAYLIB)
    InitWindow(WIDTH, HEIGHT, "Cuil example");

    SetTargetFPS(60);

    while(!WindowShouldClose()) {
        BeginDrawing();

        app_update(GetFrameTime());

        EndDrawing();
    }

    cuil_free_button(test_button);
    CloseWindow();
#else
#error "Unknown platform"
#endif
    return 0;
}

