#include <stddef.h>
#define CUIL_IMPL
#include "cuil.h"
#include "cuil_memory.h"

#ifdef CUIL_PLATFORM_RAYLIB
#include "raylib.h"
#endif

#define WIDTH  800
#define HEIGHT 600

Cuil_Button* test_button = 0;

void app_update(float dt)
{
    cuil_clear_background(0x181818FF);
    
    if (cuil_button_is_clicked(test_button)) {
        test_button->color = 0x00ff00ff;
        test_button->x = cuil_random_number_in_range(test_button->w, WIDTH-test_button->w);
        test_button->y = cuil_random_number_in_range(test_button->h, HEIGHT-test_button->h);
    } else {
        test_button->color = 0xff0000ff;
    }

    cuil_draw_button(test_button);
}

int main(void)
{
    test_button = (Cuil_Button*)malloc(sizeof(Cuil_Button));
    test_button->x = cuil_random_number_in_range(0, WIDTH);
    test_button->y = cuil_random_number_in_range(0, HEIGHT);
    test_button->w = 100;
    test_button->h = 40;
    test_button->color = 0xff0000ff;
    test_button->text = "Click me!";
    test_button->text_color = 0x000000ff;
    test_button->text_size = 15;

#if defined(CUIL_PLATFORM_WEB)
    cuil_set_app_update_func(app_update);
#elif defined(CUIL_PLATFORM_RAYLIB)
    InitWindow(WIDTH, HEIGHT, "Cuil example");

    SetTargetFPS(60);

    while(!WindowShouldClose()) {
        BeginDrawing();

        app_update(GetFrameTime());

        EndDrawing();
    }

    CloseWindow();
#else
#error "Unknown platform"
#endif


    return 0;
}

