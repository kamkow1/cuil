let wasm = undefined;
let dt = undefined;
let prev = undefined;
let ctx = undefined;
let quit = false;
let update_func = undefined;
let wasm_output = "example/styled_button.wasm";

function make_env(env) {
    return new Proxy(env, {
        get(target, prop, recv) {
            if (env[prop] !== undefined) {
                return env[prop].bind(env);
            }
            return function(...args) {
                throw new Error(`UNIMPLEMENTED: ${prop} ${args}`);
            };
        }
    });
}

function cstrlen(mem, ptr) {
    let len = 0;
    while(mem[ptr] != 0) {
        len += 1;
        ptr += 1;
    }
    return len;
}

function cstr_by_ptr(mem_buffer, ptr) {
    const mem = new Uint8Array(mem_buffer);
    const len = cstrlen(mem, ptr);
    const bytes = new Uint8Array(mem_buffer, ptr, len);
    return new TextDecoder().decode(bytes);
}

function cuil_get_window_width() {
    return ctx.canvas.width;
}

function cuil_get_window_height() {
    return ctx.canvas.height;
}

function unpack_color(color) {
    let a = (color>>0)&0xff;
    let b = (color>>1*8)&0xff;
    let g = (color>>2*8)&0xff;
    let r = (color>>3*8)&0xff;
    return [r, g, b, a];
}

function cuil_draw_pixel(x, y, color) {
    const [r, g, b, a] = unpack_color(color);
    ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${a/0xff})`;
    ctx.fillRect(x,y,1,1);
}

function cuil_draw_rect(x, y, w, h, corner_roundness, color) {
    const [r, g, b, a] = unpack_color(color);
    ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${a/0xff})`;
    if (corner_roundness > 0) {
        ctx.beginPath();
        ctx.roundRect(x, y, w, h, corner_roundness);
        ctx.stroke();
        ctx.fill();
    } else {
        ctx.fillRect(x,y,w,h);
    }
}

function cuil_draw_text(x, y, size, color, text_ptr) {
    const [r, g, b, a] = unpack_color(color);
    const buffer = wasm.instance.exports.memory.buffer;
    const text = cstr_by_ptr(buffer, text_ptr);
    const metrics = ctx.measureText(text);
    let fontHeight = metrics.fontBoundingBoxAscent + metrics.fontBoundingBoxDescent;
    let actualHeight = metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent;
    ctx.font = `${size}px monospace`;
    ctx.textAlign = "center";
    ctx.textBaseLine = "middle";
    ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${a/0xff})`;
    ctx.fillText(text, x, y+actualHeight/2);
}

let mouse_x = 0;
let mouse_y = 0;
let mouse_btn = 0;

function cuil_get_mouse_x() {
    return mouse_x;
}

function cuil_get_mouse_y() {
    return mouse_y;
}

function cuil_is_mouse_button_pressed() {
    return mouse_btn;
}

function cuil_set_app_update_func(func_ptr) {
    update_func = wasm.instance.exports.__indirect_function_table.get(func_ptr);
}

function cuil_random_number_in_range(min, max) {
    const x = Math.floor(Math.random() * (max-min) + min);
    // console.log(x);
    return x;
}

WebAssembly.instantiateStreaming(fetch(wasm_output), {
    "env": make_env({
        "cuil_set_app_update_func": cuil_set_app_update_func,
        // Window
        "cuil_get_window_width": cuil_get_window_width,
        "cuil_get_window_height": cuil_get_window_height,
        // Drawing
        "cuil_draw_pixel": cuil_draw_pixel,
        "cuil_draw_rect": cuil_draw_rect,
        "cuil_draw_text": cuil_draw_text,
        // Mouse
        "cuil_get_mouse_x": cuil_get_mouse_x,
        "cuil_get_mouse_y": cuil_get_mouse_y,
        "cuil_is_mouse_button_pressed": cuil_is_mouse_button_pressed,
        // Random numbers
        "cuil_random_number_in_range": cuil_random_number_in_range,
    })
}).then(function (w) {
    wasm = w;

    console.log(wasm);

    let app = document.getElementById("app");
    app.onmousemove = function(e) {
        let rect = e.target.getBoundingClientRect();
        mouse_x = Math.floor(e.clientX - rect.left);
        mouse_y = Math.floor(e.clientY - rect.top);
    }
    app.onmousedown = function(e) {
        switch(e.button) {
        case 0: mouse_btn = 1; break;
        case 1: mouse_btn = 4; break;
        case 2: mouse_btn = 2; break;
        }
    }
    app.onmouseup = function(e) {
        mouse_btn = 0;
    }

    ctx = app.getContext("2d");

    wasm.instance.exports.main();

    const next = function (timestamp) {
        if (quit) return;
        dt = (timestamp - prev)/1000.0;
        prev = timestamp;
        update_func();
        window.requestAnimationFrame(next);
    };
    window.requestAnimationFrame(function (timestamp) {
        prev = timestamp;
        window.requestAnimationFrame(next);
    });
});
