#ifndef CUIL_H_
#define CUIL_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define CUIL_HOSTFUNC extern
#define CUIL_DEF static inline

void* malloc(size_t);
void free(void*);

typedef int Cuil_Color;

// Cuil color palette
#define CUIL_GRAY 0x7f7582FF
#define CUIL_VIOLET 0x753464FF
#define CUIL_RED 0xb72930FF
#define CUIL_BLUE 0x4154b9FF
#define CUIL_PURPLE 0x53409aFF
#define CUIL_WHITE 0xf3efdbFF
#define CUIL_PINK 0xe76890FF
#define CUIL_ORANGE 0xeb9860FF
#define CUIL_GREEN 0x94b551FF
#define CUIL_BROWN 0xb67a4eFF
#define CUIL_YELLOW 0xffc435FF
#define CUIL_DARK_OCEAN 0x005a79FF
#define CUIL_DARK_GRAY 0x181818FF
#define CUIL_DARK_PINK 0x3f5724FF
#define CUIL_LIGHT_GREEN 0x9fbe00FF
#define CUIL_LIGHT_GRAY 0xbfb1acFF
#define CUIL_LIGHT_BLUE 0xb3efe2FF

typedef enum {
    CUIL_MOUSE_LEFT = 1,
    CUIL_MOUSE_RIGHT = 2,
    CUIL_MOUSE_MIDDLE = 4,
} Cuil_Mouse_Flags;

typedef void(*Cuil_App_Update_Func)(float);

CUIL_HOSTFUNC void cuil_set_app_update_func(Cuil_App_Update_Func func);

CUIL_HOSTFUNC size_t cuil_get_window_width(void);
CUIL_HOSTFUNC size_t cuil_get_window_height(void);

CUIL_HOSTFUNC size_t cuil_get_mouse_x(void);
CUIL_HOSTFUNC size_t cuil_get_mouse_y(void);
CUIL_HOSTFUNC uint8_t cuil_is_mouse_button_pressed(void);

CUIL_DEF void cuil_clear_background(Cuil_Color color);
CUIL_HOSTFUNC void cuil_draw_pixel(size_t x, size_t y, Cuil_Color color);
CUIL_HOSTFUNC void cuil_draw_rect(size_t x, size_t y, size_t w, size_t h, size_t corner_roundness, Cuil_Color color);
CUIL_HOSTFUNC void cuil_draw_text(size_t x, size_t y, size_t size, Cuil_Color color, const char* text);

CUIL_HOSTFUNC int cuil_random_number_in_range(int min, int max);


typedef struct {
    size_t x, y, w, h;
    Cuil_Color color_primary;
    Cuil_Color color_secondary;
    bool flip_color;
    Cuil_Color text_color;
    const char* text;
    size_t text_size;
    size_t corner_roundness;
} Cuil_Button;

CUIL_DEF Cuil_Button* cuil_make_button(size_t x, size_t y, size_t w, size_t h, const char* text); // allocates the button, caller has to free the memory
CUIL_DEF void cuil_free_button(Cuil_Button* button); // frees button's memory
CUIL_DEF void cuil_style_button(Cuil_Button* button); // gives a default style to a button
CUIL_DEF void cuil_draw_button(Cuil_Button* button);
CUIL_DEF bool cuil_button_is_mouse_over(Cuil_Button* button);
CUIL_DEF bool cuil_button_is_clicked(Cuil_Button* button);

#ifdef CUIL_IMPL

CUIL_DEF void cuil_clear_background(Cuil_Color color)
{
    size_t w = cuil_get_window_width();
    size_t h = cuil_get_window_height();
    cuil_draw_rect(0, 0, w, h, 0, color);
}

CUIL_DEF void cuil_style_button(Cuil_Button* button)
{
    button->color_primary = CUIL_GRAY;
    button->color_secondary = CUIL_LIGHT_GRAY;
    button->text_color = CUIL_DARK_GRAY;
    button->corner_roundness = 5;
    button->text_size = 15;
}

CUIL_DEF Cuil_Button* cuil_make_button(size_t x, size_t y, size_t w, size_t h, const char* text)
{
    Cuil_Button *btn = (Cuil_Button*)malloc(sizeof(Cuil_Button));
    btn->x = x;
    btn->y = y;
    btn->w = w;
    btn->h = h;
    btn->text = text;
    cuil_style_button(btn);
    return btn;
}

CUIL_DEF void cuil_free_button(Cuil_Button* button)
{
    free(button);
}

CUIL_DEF void cuil_draw_button(Cuil_Button* button)
{
    cuil_draw_rect(button->x, button->y, button->w, button->h, button->corner_roundness,
                    !button->flip_color ? button->color_primary : button->color_secondary);
    cuil_draw_text(button->x+(button->w/2), button->y+(button->h/2),
                    button->text_size, button->text_color, button->text);
}

CUIL_DEF bool cuil_button_is_mouse_over(Cuil_Button* button)
{
    size_t x = cuil_get_mouse_x();    
    size_t y = cuil_get_mouse_y();
    return (x >= button->x && x <= button->x + button->w)
        && (y >= button->y && y <= button->y + button->h);

}

CUIL_DEF bool cuil_button_is_clicked(Cuil_Button* button)
{
    return cuil_button_is_mouse_over(button)
        && cuil_is_mouse_button_pressed() & CUIL_MOUSE_LEFT;
}


#endif // CUIL_IMPL

#endif // CUIL_H_
