#define MIBS_IMPL
#define MIBS_CC "clang-14" // must be clang for cross compilation
#include "mibs.h"

Mibs_Default_Allocator allocator = mibs_make_default_allocator();

typedef enum {
    CMD_BUILD,
    CMD_CLEAN,
} Command;
static const char* string_commands[] = {"build", "clean"};

typedef enum {
    EXAMPLE_WEB,
} Example;
static const char* string_examples[] = {"web"};

void help(void)
{
    mibs_log(MIBS_LL_INFO, "Available commands:\n");
    for (size_t i = 0; i < mibs_array_len(string_commands); i++) {
        mibs_log(MIBS_LL_INFO, "    -C=%s\n", string_commands[i]);
    }
}

#define web_flags \
    "--target=wasm32", \
    "-Os", \
    "--no-standard-libraries", \
    "-Wl,--export-table", \
    "-Wl,--no-entry", \
    "-Wl,--allow-undefined", \
    "-Wl,--export=main", \
    "-I."

#define raylib_flags \
    "-L./raylib-5.0_linux_amd64/lib", \
    "-l:libraylib.a", \
    "-lglfw", \
    "-lm", \
    "-ldl", \
    "-lpthread", \
    "-I./raylib-5.0_linux_amd64/include", \
    "-I."

typedef enum {
    PLATFORM_WEB,
    PLATFORM_RAYLIB,
} Platform;
static const char* string_platforms[] = {"web", "raylib"};

bool build_example(Platform platform, const char* example)
{
    Mibs_String_Builder example_sb = {0};
    mibs_sb_append_cstr(&allocator, &example_sb, "example/");
    mibs_sb_append_cstr(&allocator, &example_sb, example);
    mibs_sb_append_cstr(&allocator, &example_sb, ".c");
    mibs_sb_append_null(&allocator, &example_sb);
    
    Mibs_Cmd build_cmd = {0};
    switch(platform) {
    case PLATFORM_RAYLIB:
        {
            Mibs_String_Builder output_sb = {0};
            mibs_sb_append_cstr(&allocator, &output_sb, "example/");
            mibs_sb_append_cstr(&allocator, &output_sb, example);
            mibs_sb_append_null(&allocator, &output_sb);

            mibs_cmd_append(&allocator, &build_cmd, MIBS_CC);
            mibs_cmd_append(&allocator, &build_cmd, "-o");
            mibs_da_append(&allocator, &build_cmd, output_sb.items);
            mibs_cmd_append(&allocator, &build_cmd, "-DCUIL_PLATFORM_RAYLIB");
            mibs_cmd_append(&allocator, &build_cmd, "cuil_raylib.c");
            mibs_da_append(&allocator, &build_cmd, example_sb.items);
            mibs_cmd_append(&allocator, &build_cmd, raylib_flags);
        } break;
    case PLATFORM_WEB:
        {
            Mibs_String_Builder output_sb = {0};
            mibs_sb_append_cstr(&allocator, &output_sb, "example/");
            mibs_sb_append_cstr(&allocator, &output_sb, example);
            mibs_sb_append_cstr(&allocator, &output_sb, ".wasm");
            mibs_sb_append_null(&allocator, &output_sb);

            mibs_cmd_append(&allocator, &build_cmd, MIBS_CC);
            mibs_cmd_append(&allocator, &build_cmd, web_flags);
            mibs_cmd_append(&allocator, &build_cmd, "-o");
            mibs_da_append(&allocator, &build_cmd, output_sb.items);
            mibs_cmd_append(&allocator, &build_cmd, "-DCUIL_PLATFORM_WEB");
            mibs_cmd_append(&allocator, &build_cmd, "walloc.c");
            mibs_da_append(&allocator, &build_cmd, example_sb.items);
        } break;
    default:
        mibs_log(MIBS_LL_ERROR, "Unhandled platform %d\n", platform);
        return false;
    }

    bool result = mibs_run_cmd(&allocator, &build_cmd, MIBS_CMD_SYNC, NULL).ok;
    mibs_da_deinit(&allocator, &build_cmd);
    mibs_da_deinit(&allocator, &example_sb);
    return result;
}

bool clean(void)
{
    Mibs_Cmd cmd = {0};
    mibs_cmd_append(&allocator, &cmd, "rm", "-f");

    Mibs_Read_File_Result rf_result = mibs_read_file(&allocator, "./.gitignore");
    if (!rf_result.ok) return false;

    Mibs_String_Builder file_sb = rf_result.value;
   
    Mibs_String_Array lines = mibs_split_cstr(&allocator, file_sb.items, "\n");
    for (size_t i = 0; i < lines.count; i++) {
        mibs_da_append(&allocator, &cmd, lines.items[i]);
    }

    if (!mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok) return false;

    mibs_da_deinit(&allocator, &file_sb);
    mibs_da_deinit(&allocator, &cmd);
    return true;
}

int main(int argc, char** argv)
{
    mibs_rebuild(&allocator, argc, argv);
    Mibs_Options options = mibs_options(&allocator, argc, argv);

    const char* command = mibs_extract_option("C", MIBS_OV_STRING, {
        mibs_log(MIBS_LL_ERROR, "No command provided\n");
        return 1;
    });

    if (mibs_compare_cstr(command, string_commands[CMD_BUILD])) {
        const char* platform_str = mibs_extract_option("platform", MIBS_OV_STRING, {
            mibs_log(MIBS_LL_ERROR, "No platform provided\n");
            return 1;
        });
        Platform platform = -1;
        for (size_t i = 0; i < mibs_array_len(string_platforms); i++) {
            const char* platform_name = string_platforms[i];
            if (mibs_compare_cstr(platform_name, platform_str)) {
                platform = i;
            }
        }
        if (platform == -1) {
            mibs_log(MIBS_LL_ERROR, "Unknown platform %s\n", platform_str);
            return 1;
        }

        const char* example = mibs_extract_option("example", MIBS_OV_STRING, {
            mibs_log(MIBS_LL_ERROR, "No example provided\n");
            return 1;
        });

        if (!build_example(platform, example)) return 1;
    } else if (mibs_compare_cstr(command, string_commands[CMD_CLEAN])) {
        if (!clean()) return 1;
    } else {
        mibs_log(MIBS_LL_ERROR, "Unknown command %s\n", command);
        return 1;
    }

    return 0;
}
