#include <stdlib.h>
#include "cuil.h"
#include "raylib.h"

Color unpack_color(Cuil_Color color)
{
    unsigned char a = (color>>0)&0xff;
    unsigned char b = (color>>1*8)&0xff;
    unsigned char g = (color>>2*8)&0xff;
    unsigned char r = (color>>3*8)&0xff;
    return (Color){ .r = r, .g = g, .b = b, .a = a };
}

size_t cuil_get_window_width(void)
{
    return GetScreenWidth();
}

size_t cuil_get_window_height(void)
{
    return GetScreenHeight();
}

size_t cuil_get_mouse_x(void)
{
    return GetMousePosition().x;
}

size_t cuil_get_mouse_y(void)
{
    return GetMousePosition().y;
}

void cuil_draw_pixel(size_t x, size_t y, Cuil_Color color)
{
    DrawPixel(x, y, unpack_color(color));
}

void cuil_draw_rect(size_t x, size_t y, size_t w, size_t h, size_t corner_roundness, Cuil_Color color)
{
    if (corner_roundness > 0) {
        Rectangle rec = {x, y, w, h};
        DrawRectangleRounded(rec, corner_roundness*0.06, 0.0f, unpack_color(color));
    } else {
        DrawRectangle(x, y, w, h, unpack_color(color));
    }
}

void cuil_draw_text(size_t x, size_t y, size_t size, Cuil_Color color, const char* text)
{
    int text_width = MeasureText(text, size);
    DrawText(text, x-(text_width/2), y-(size/2), size, unpack_color(color));
}

uint8_t cuil_is_mouse_button_pressed(void)
{
    uint8_t flags = 0;
    if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
        flags |= CUIL_MOUSE_LEFT;
    }
    if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT)) {
        flags |= CUIL_MOUSE_RIGHT;
    }
    if (IsMouseButtonPressed(MOUSE_BUTTON_MIDDLE)) {
        flags |= CUIL_MOUSE_MIDDLE;
    }
    return flags;
}

int cuil_random_number_in_range(int min, int max)
{
    return (int)rand()%(max-min)+min;
}
